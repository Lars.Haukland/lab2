package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
	
	ArrayList<FridgeItem> items = new ArrayList<>();
	
	int maxCapasity = 20;
	
	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		return this.items.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return this.maxCapasity;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		// TODO Auto-generated method stub
		if(this.nItemsInFridge() < 20) {
			this.items.add(item);
			return true;
		}
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		// TODO Auto-generated method stub
		
		boolean removed = false;
		
		if (this.items.contains(item)) {
			this.items.remove(item);
			removed = true;
		}
		
		if (!removed) {
			throw new NoSuchElementException(); 
		}
		
	}

	@Override
	public void emptyFridge() {
		// TODO Auto-generated method stub
		this.items.removeAll(items);
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		// TODO Auto-generated method stub
		ArrayList<FridgeItem> expiredFood = new ArrayList<>();
		
		for(FridgeItem item : this.items) {
			if(item.hasExpired()) {
				expiredFood.add(item);
			}
		}
		
		for (FridgeItem expired : expiredFood) {
			this.takeOut(expired);
		}
		
		return expiredFood;
	}
	
}
